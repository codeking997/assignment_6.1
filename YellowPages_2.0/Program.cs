﻿using System;
using System.Collections.Generic;

namespace YellowPages_2._0
{
    class Program
    {
        static List<Person> personList = new List<Person>();
        static void Main(string[] args)
        {
            Search();
            

        }
        //populates the list
        private static void ListPopulate()
        {
            

            personList.Add(new Person("Napoleon", "Bonaparte", 55384858));
            personList.Add(new Person("Jean-Baptiste", "Bernadotte", 56374858));
            personList.Add(new Person("Julius", "Cæsar", 97856310));
            personList.Add(new Person("Sigurd", "Jorsalfar", 534877));
            personList.Add(new Person("Aleksandr", "Suvorov", 56444858));

        }
        //gives the instructions to the user
        private static void Instructions()
        {
            Console.WriteLine("Welcome search through the list by typing something");
        }
        /*this method's while loop goes through the list of persons and checks if the input match any of
         * the persons
         */
        private static void Search()
        {
            
            ListPopulate();  

            while (true)
            {
                Instructions();
                string input = Console.ReadLine();
                for (int i = 0; i < personList.Count; i++)
                {
                    /*
                     * made a change here from original commit because I saw that on certain occations the search
                     * function would not return results when for example letters were in uppercase. 
                     */
                    if (personList[i].firstName.Contains(input, StringComparison.CurrentCultureIgnoreCase)|| personList[i].lastName.Contains(input, StringComparison.CurrentCultureIgnoreCase))
                    {
                        Console.WriteLine($"{personList[i].firstName} {personList[i].lastName}, {personList[i].phoneNumber}");
                        
                    }//this makes it possible to search by phone number
                    else if (personList[i].phoneNumber.ToString().Contains(input))
                    {
                        Console.WriteLine($"{personList[i].firstName} {personList[i].lastName}, {personList[i].phoneNumber}");
                    }


                }
            }
        }
    }
}
